#PVSim a python photo-voltaic simulator

```
$ pvsim -h
usage: pvsim [-h] [-v | -q] [-f FILE] [-a] [-c CONNECT]              
                                                                      
optional arguments:                                                   
  -h, --help            show this help message and exit               
  -v, --verbose         Log debug information to stdout               
  -q, --quiet           Only output warnings and errors               
  -f FILE, --file FILE  Log file to write                             
  -a, --append          Do not delete the log, just append new records
  -c CONNECT, --connect CONNECT                                       
                        ampq:// url to use for the broker             
```

##Deps (others are specified in `setup.py`):
```
$ sudo apt install -yq rabbitmq-server python3-pip python3-matplotlib
```


##Tests:
```
$ pip3 install tox
$ tox
```

##Installation:
* Optionally do this in a virtualenv to run as non-root and keep your system pristine
    - Can be run either via setuptools OR pip
```
$ sudo python3 ./setup.py install # to use setuptools
```
```
$ sudo pip3 install . # to use pip
```

##Running:
* Make sure you have rabbitmq installed and running
    - Check by running `sudo rabbitmqctl list_queues`
    - This code defaults to connecting to "amqp://guest:guest@localhost/", so make sure that is valid
* It may take a while (~2min), to check progress you can `tail -F <logfile>`
    - Default logfile name is `pvsim.log`
```
$ pvsim
```
* An additional command is installed as well `pvgraph`
    - It takes no options and just makes a png in the current directory of the pvsim.log
    - Feel free to open the pvsim.png in any image viewer you like
```
$ pvgraph
```
