import random
import logging
from pvsim.broker.client import Consumer
from pvsim.meter import Reading
from pvsim.times import TODAY_SUNRISE, TODAY_SUNSET

logger = logging.getLogger(__name__)


class PVSim(Consumer):
    """A Meter class that sends meter readings to the broker"""
    def __init__(self, *args, sunrise=TODAY_SUNRISE, sunset=TODAY_SUNSET,
                 max_voltage=3300, max_jitter=None, log='pvsim.log', **kwargs):
        self.sunrise = sunrise
        self.sunset = sunset
        self.max_voltage = max_voltage
        logger.info('Opening log file %s for writing', log)
        self.log_handle = open(log, mode='a')
        if max_jitter is None:
            self.max_jitter = self.max_voltage / 1500
        else:
            self.max_jitter = max_jitter
        super(PVSim, self).__init__(*args, **kwargs)

    def random_pv_voltage(self, now):
        """Return an approximation of a random voltage for a certain time.
        Use the sunrise and sunset to determine percentage of the sun showing.
        Then just squareroot and add jitter.

        This makes the sun a square that slowly gets coverage from a corner
        spreading out."""
        if now < self.sunrise or now > self.sunset:
            return 0
        half_day = (self.sunset - self.sunrise) // 2
        solar_noon = self.sunrise + half_day
        jitter = random.uniform(self.max_jitter * -1, self.max_jitter)
        percent_shade = abs(
            (now - solar_noon).total_seconds() / half_day.total_seconds()
        )
        percent_sunshine = 1 - percent_shade
        # XXX: should use a circle rather than square approximation for the sun
        return (percent_sunshine ** 0.5) * self.max_voltage + jitter

    def __del__(self):
        self.log_handle.close()

    def log(self, reading, pv_voltage):
        """Write the given Reading and pv_voltage to the log"""
        output = (reading.time.isoformat(), reading.voltage,
                  pv_voltage, reading.voltage + pv_voltage)
        log_entry = '%s: %f %f %f\n' % (output)
        self.log_handle.write(log_entry)
        self.log_handle.flush()
        logger.debug('Wrote %s to log', log_entry.strip())

    def consume_message(self, message):
        """Consume the message, create random pv_voltage for the time and
        write it to the log. Will be run for each message in the queue."""
        with message.process():
            logger.debug('Received %s', message.body.decode().strip())
            reading = Reading.deserialize(message.body.decode())
            pv_voltage = self.random_pv_voltage(reading.time)
            self.log(reading, pv_voltage)
