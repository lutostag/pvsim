import matplotlib
matplotlib.use('Agg')
import matplotlib.dates as mdates
import numpy as np
import pylab


def main():
    time, meter, pv_sim, total = np.loadtxt(
        "pvsim.log", unpack=True,
        converters={0: mdates.bytespdate2num('%Y-%m-%dT%H:%M:%S:')})

    pylab.plot_date(x=time, y=meter, c='r', linestyle='-')
    pylab.plot_date(x=time, y=pv_sim, c='g', linestyle='-')
    pylab.plot_date(x=time, y=total, c='b', linestyle='-')
    pylab.savefig('pvsim.png')


if __name__ == '__main__':
    main()
