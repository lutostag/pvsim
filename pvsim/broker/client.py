import asyncio
import logging
import typing
import pika
from aio_pika import connect, IncomingMessage, Message, ExchangeType

logger = logging.getLogger(__name__)

EXCHANGE = 'message'
EXCHANGE_TYPE = ExchangeType.TOPIC
QUEUE = 'meter'
ROUTING_KEY = 'pvsim.meter'


class Messenger(object):
    """Base class for dealing with amqp connections"""

    def __init__(self, url, loop):
        """url to connect to, e.g. "amqp://guest:guest@localhost/" and
        event loop to process the async calls on"""
        self.url = url
        self.loop = loop

    async def connect_channel(self):
        """Connect to the remote amqp endpoint and setup a channel"""
        self.connection = await connect(url=self.url, loop=self.loop)

        # Creating a channel
        self.channel = await self.connection.channel()

    async def close(self):
        """Actually close the channel and connection correctly for the last
        time"""
        await self.channel.close()
        await self.connection.close()
        logger.debug('Connection closed')

    async def connect(self):
        """Connect to the set exchange and queue and bind to the appropriate
        routing_key. This should be called before any consumer/producer to
        ensure proper setup."""
        await self.connect_channel()
        await self.channel.set_qos(prefetch_count=1)

        self.exchange = await self.channel.declare_exchange(
            EXCHANGE, EXCHANGE_TYPE, durable=True, auto_delete=True)

        self.queue = await self.channel.declare_queue(
            QUEUE, durable=True, exclusive=False, auto_delete=True)

        await self.queue.bind(self.exchange, ROUTING_KEY)


class Consumer(Messenger):
    """Wrapper class to abstract message consuming"""

    async def process(self):
        """Consume messages from the queue and send them to the consume_message
        callback."""
        # Perform connection
        await self.connect()
        await asyncio.sleep(2)

        # Start listening the queue
        logger.info('Consuming %s', self.queue)
        await self.queue.consume(self.consume_message)

    def consume_message(self, message: IncomingMessage):
        """An example callback that processes messages."""
        with message.process():
            logger.debug('Recieving %s', message)
            print("[x] %r" % message.body)


class Producer(Messenger):
    """Wrapper class to abstract message publishing"""

    async def destroy_routes(self):
        """Repeatedly ask to delete the queue and exchange if it is empty.
        Return on success."""
        logger.debug('Starting teardown attempt')
        deleted = False
        while not deleted:
            try:
                # On queue_delete failure the channel is closed with an error
                # that the queue is not empty.
                await self.channel.queue_delete(QUEUE, if_empty=True)
                await self.channel.exchange_delete(EXCHANGE)
                deleted = True
            except pika.exceptions.ChannelClosed:
                # because the channel is closed, we must reopen to attempt to
                # delete the remote objects again for the loop's next cycle
                await self.connection.close()
                await self.connect_channel()
                await asyncio.sleep(1)

        logger.debug('Teardown complete')

    async def process(self, teardown=True):
        """Pull messages from create_message and publish them to the exchange.
        If teardown is True, both the queue and exchange will be deleted from
        the server after the last message has been drained from the queue.
        """
        await self.connect()
        logger.info('Producing %s', self.exchange)

        for message in self.create_messages():
            logger.debug('Sending %s', message.strip())
            await self.exchange.publish(
                Message(body=message.encode()),
                routing_key=ROUTING_KEY
            )

        if teardown:
            await self.destroy_routes()

    def create_messages(self) -> typing.Iterator[str]:
        """An example generator of messages to be sent."""
        for i in range(16):
            yield 'Hello, {}'.format(ROUTING_KEY)


def _exception_handler(loop, context):
    pass


def run_loop(loop, *messengers):
    # exception handler should be set to not clutter the cli, as aio-pika does
    # not always properly handle async errors
    loop.set_exception_handler(_exception_handler)
    try:
        # run process for each messenger
        tasks = [loop.create_task(messenger.process())
                 for messenger in messengers]
        waitable = asyncio.gather(*tasks, return_exceptions=True)
        loop.run_until_complete(waitable)
        result = waitable.result()
        # explicitly close the messengers to avoid unclosed async
        # connections
        closers = [messenger.close() for messenger in messengers]
        loop.run_until_complete(
            asyncio.gather(*closers, return_exceptions=True))
        return result
    finally:
        loop.close()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    consumer = Consumer("amqp://guest:guest@localhost/", loop=loop).process()
    producer_task1 = Producer("amqp://guest:guest@localhost/",
                              loop=loop).process()
    producer_task2 = Producer("amqp://guest:guest@localhost/",
                              loop=loop).process()
    run_loop(loop, consumer, producer_task1, producer_task2)
