from datetime import datetime, timedelta

TODAY_MIDNIGHT = datetime.now().replace(
    hour=0, minute=0, second=0, microsecond=0)
TOMORROW_MIDNIGHT = TODAY_MIDNIGHT + timedelta(days=1)
DEFAULT_STEP = timedelta(seconds=2)

TODAY_SUNRISE = TODAY_MIDNIGHT + timedelta(hours=5, minutes=12)
# assume an equinox with 12-hours of sunshine
TODAY_SUNSET = TODAY_SUNRISE + timedelta(hours=12)


def create_timerange(start=TODAY_MIDNIGHT, end=TOMORROW_MIDNIGHT,
                     step=DEFAULT_STEP):
    """Generator for a timerange where the endpoints are inclusive."""
    assert(start <= end and step > timedelta(0) or
           start >= end and step < timedelta(0))
    current = start
    while (current <= end):
        yield current
        current += step
