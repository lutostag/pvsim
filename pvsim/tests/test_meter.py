from unittest import TestCase
from pvsim import meter
from pvsim import times
from datetime import datetime, timedelta


class TestReading(TestCase):
    def test_serialization(self):
        time = datetime.now()
        serialization = meter.Reading(time=time, voltage=0.9).serialize()
        self.assertIsInstance(serialization, str)

    def test_deserialization(self):
        time = datetime.now()
        reading = meter.Reading(time=time, voltage=0.9)
        self.assertEqual(reading,
                         meter.Reading.deserialize(reading.serialize()))


class TestMeter(TestCase):
    def test_generate_messages(self):
        time_range = times.create_timerange(step=timedelta(hours=12))
        m = meter.Meter(url=None, loop=None,
                        time_range=time_range, voltage_range=range(0, 1))
        count = 0
        for reading in m.meter():
            self.assertIsInstance(reading, str)
            reading = meter.Reading.deserialize(reading)
            self.assertTrue(0 <= reading.voltage and reading.voltage <= 1)
            print(reading.time)
            count += 1
        self.assertEqual(count, 3)
