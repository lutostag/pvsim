from unittest import TestCase
from pvsim import times
from datetime import timedelta


class TestTimes(TestCase):
    def test_create_timerange_fails(self):
        time_range = times.create_timerange(step=timedelta(seconds=-1))
        # AssertionErrors cannot be used in assertRaises
        try:
            next(time_range)
            self.assertTrue(False)
        except AssertionError:
            self.assertTrue(True)

    def test_create_timerange_ends(self):
        delta = timedelta(hours=12)
        time_range = times.create_timerange(step=delta)
        items = list(time_range)
        self.assertEqual(len(items), 3)
        self.assertEqual(items[1] - items[0], delta)
        self.assertEqual(items[-1] - items[0], timedelta(days=1))
