from unittest import TestCase
from datetime import timedelta
from pvsim import pv_sim
from pvsim import times


class TestPVSim(TestCase):
    def test_random_voltage_low(self):
        sim = pv_sim.PVSim(url=None, loop=None, max_voltage=1, max_jitter=1)
        self.assertEquals(sim.random_pv_voltage(times.TODAY_MIDNIGHT), 0)

    def test_random_voltage_peak(self):
        sim = pv_sim.PVSim(url=None, loop=None, sunrise=times.TODAY_MIDNIGHT,
                           sunset=times.TOMORROW_MIDNIGHT, max_voltage=1,
                           max_jitter=0)
        noon = times.TODAY_MIDNIGHT + timedelta(hours=12)
        self.assertEquals(sim.random_pv_voltage(noon), 1)

    def test_random_voltage_increasing(self):
        sim = pv_sim.PVSim(url=None, loop=None, max_voltage=1,
                           max_jitter=0)
        one = times.TODAY_MIDNIGHT + timedelta(hours=8)
        two = times.TODAY_MIDNIGHT + timedelta(hours=11)
        self.assertTrue(
            sim.random_pv_voltage(one) < sim.random_pv_voltage(two))

    def test_random_voltage_decreasing(self):
        sim = pv_sim.PVSim(url=None, loop=None, max_voltage=1,
                           max_jitter=0)
        one = times.TODAY_MIDNIGHT + timedelta(hours=14)
        two = times.TODAY_MIDNIGHT + timedelta(hours=16)
        self.assertTrue(
            sim.random_pv_voltage(one) > sim.random_pv_voltage(two))
