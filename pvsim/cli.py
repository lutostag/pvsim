import argparse
import asyncio
import logging
import os
from pvsim.meter import Meter
from pvsim.pv_sim import PVSim
from pvsim.broker import client

CONNECTION = "amqp://guest:guest@localhost/"


def set_logging(opts):
    level = logging.INFO
    if opts.quiet:
        level = logging.WARNING
    elif opts.verbose:
        level = logging.DEBUG

    logging.basicConfig(level=level)
    # hide logging from these packages as it would be too noisy with them
    logging.getLogger("aio_pika").setLevel(99)
    logging.getLogger("pika").setLevel(99)


def remove_old_log(opts):
    if opts.append:
        return
    try:
        os.remove(opts.file)
    except FileNotFoundError:
        pass


def parse_args():
    parser = argparse.ArgumentParser()
    verbosity = parser.add_mutually_exclusive_group()
    verbosity.add_argument('-v', '--verbose', action='store_true',
                           help='Log debug information to stdout')
    verbosity.add_argument('-q', '--quiet', action='store_true',
                           help='Only output warnings and errors')
    parser.add_argument('-f', '--file', default='pvsim.log',
                        help='Log file to write')
    parser.add_argument('-a', '--append', action='store_true',
                        help='Do not delete the log, just append new records')
    parser.add_argument('-c', '--connect', default=CONNECTION,
                        help='ampq:// url to use for the broker')

    return parser.parse_args()


def main():
    opts = parse_args()
    set_logging(opts)
    remove_old_log(opts)
    loop = asyncio.get_event_loop()
    meter = Meter(opts.connect, loop=loop)
    pv = PVSim(opts.connect, loop=loop, log=opts.file)
    client.run_loop(loop, meter, pv)


if __name__ == '__main__':
    main()
