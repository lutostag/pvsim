import yaml
import attr
import random
from pvsim.broker.client import Producer
from pvsim.times import create_timerange


@attr.s
class Reading(object):
    """A meter reading"""
    time = attr.ib()
    voltage = attr.ib()

    def serialize(self) -> str:
        return yaml.safe_dump(self.__dict__)

    @classmethod
    def deserialize(cls, serialization: str):
        dict_ = yaml.safe_load(serialization)
        return cls(**dict_)


class Meter(Producer):
    """A Meter class that sends meter readings to the broker"""
    def __init__(self, *args, time_range=None, voltage_range=None, **kwargs):
        if time_range is None:
            time_range = create_timerange()
        if voltage_range is None:
            voltage_range = range(0, 9000)
        self.time_range = time_range
        self.voltage_range = voltage_range
        super(Meter, self).__init__(*args, **kwargs)

    def meter(self):
        for time in self.time_range:
            random_voltage = random.uniform(
                self.voltage_range.start, self.voltage_range.stop)
            yield Reading(time=time, voltage=random_voltage).serialize()

    def create_messages(self):
        yield from self.meter()
