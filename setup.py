#!/usr/bin/env python3
import os
from setuptools import setup, find_packages

setup(
    name='pvsim',
    version='0.0.1',
    description='Simulator for photovoltaic power',
    author='Greg Lutostanski',
    author_email='greg.luto@gmail.com',
    url='',
    packages=find_packages(),
    classifiers=['Programming Language :: Python :: 3.5'],
    install_requires=[
        'PyYAML',
        'multidict==3.1.3',
        'aio-pika',
        'matplotlib',
        'attrs',
    ],
    entry_points={
        'console_scripts': ['pvsim = pvsim.cli:main',
                            'pvgraph = pvsim.graph:main']
    }
)
